from django.shortcuts import render_to_response 
from django.template import RequestContext 
from lang1.models import Language

def home(request): 
	ctx=RequestContext(request) 
	temp_name="index.html"
	values=Language.objects.all()
	print values

	response=render_to_response(temp_name,locals(),ctx) 
	return response
