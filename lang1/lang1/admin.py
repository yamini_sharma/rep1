from django.contrib import admin
from django.db import models
from lang1.models import Language,Writer

admin.site.register(Language)
admin.site.register(Writer)